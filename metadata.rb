name             'gitlab_redis_backup'
maintainer       'GitLab Inc.'
maintainer_email 'ops-contact+cookbooks@gitlab.com'
license          'MIT'
description      'Run Redis RDB backups on replicas, not primary node, when possible'
version          '0.2.0'
chef_version     '>= 14.0'
issues_url       'https://gitlab.com/gitlab-cookbooks/gitlab_redis_backup/issues'
source_url       'https://gitlab.com/gitlab-cookbooks/gitlab_redis_backup'

supports 'ubuntu', '= 16.04'

# Please specify dependencies with version pin:
# depends 'cookbookname', '~> 1.0.0'
