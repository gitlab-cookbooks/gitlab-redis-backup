#!/bin/bash

# Usage:
# $ redis_backup.sh
# $ redis_backup.sh [conf_file]
#
# Run this script periodically via cron or systemd timer.
# Its default config file is:
#   /etc/gitlab/redis_backup.conf
#
# Purpose:
# Periodically run Redis RDB backups, but only on non-primary nodes.
# If the local node is currently in the primary role, skip the backup
# as long as at least one replica is healthy and has tolerable replication lag.
#
# With this in place, we can reduce or remove the SAVE directive in redis.conf,
# to avoid redis latency spikes.
#
# Background:
# Creating an RDB backup induces a latency spike for Redis clients.
# This is caused by copy-on-write overhead for memory pages shared between
# the main redis-server process and its forked child process that creates
# the RDB backup file.  To avoid causing client-facing latency spikes,
# we can suppress making backups on the primary node, delegating that work
# to only run on replica nodes.  This script does that.
#
# When this script runs periodically, we can either replace or supplement
# the redis config directive:
#   SAVE <seconds> <changes>
# Keeping a SAVE directive with a very low frequency (e.g. daily) would ensure
# that redis backups are still made occasionally even if this script fails
# (e.g. auth errors) or is disabled (e.g. cron stopped).
#
# For more details, see:
# https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/566

CONFIG_FILE=${1:-/etc/gitlab/redis_backup.conf}

# We are intentionally not setting "errexit", because we do not want a recurring error
# to inadvertently cause backups to repeatedly be skipped on all nodes.
# However, we do set "pipefail" so that if any command in a chain of pipes exits non-zero,
# the whole pipeline does too.
set -o pipefail

function main()
{
    load_config
    skip_backup_if_is_redis_not_running
    skip_backup_if_redis_is_primary_with_a_healthy_replica
    skip_backup_if_recently_completed_last_successful_backup
    run_backup
}

function load_config()
{
    [[ -r "$CONFIG_FILE" ]] || die "Config file is missing or not readable: $CONFIG_FILE"
    . "$CONFIG_FILE"

    is_number "$MAX_ALLOWED_REPLICATION_LAG_SECONDS" || die "Config error: MAX_ALLOWED_REPLICATION_LAG_SECONDS must be >= 0"
    is_number "$MIN_SECONDS_SINCE_LAST_SUCCESSFUL_BACKUP" || die "Config error: MIN_SECONDS_SINCE_LAST_SUCCESSFUL_BACKUP must be >= 0"
    [[ -x "$REDIS_CLI" ]] || die "Config error: REDIS_CLI is missing or not executable: '$REDIS_CLI'"
    [[ -r "$REDIS_CONF" ]] || die "Config error: REDIS_CONF is missing or not readable: '$REDIS_CONF'"
}

function is_number()
{
    local value=$1
    local regex='^[0-9][0-9]*$'
    [[ $value =~ $regex ]]
}

function skip_backup_if_is_redis_not_running()
{
    # If Redis is not running, it is of course not possible to make a backup of its memory state.
    # In that case, just log the circumstances and exit.

    pidof 'redis-server' >& /dev/null || skip_backup "Redis does not appear to be running."
}

function skip_backup_if_redis_is_primary_with_a_healthy_replica()
{
    # If the local redis instance is the primary and it has at least 1 healthy replica, then
    # do not run a local backup.  When its replicas run this script, they will make backups.
    # The only time we need to force the primary to make a backup is when it has no healthy replicas.
    # For our purposes, "healthy" is defined as the replication lag being less than X seconds.

    is_redis_primary && redis_has_healthy_replica && skip_backup "Redis role is primary and at least one replica is healthy."
}

function is_redis_primary()
{
    run_redis_command --raw role | head -n 1 | grep 'master' >& /dev/null
}

function redis_has_healthy_replica()
{
    # Only the redis primary node will include a list of replicas in its INFO output.
    # Each replica status will include a "lag=X" field that measures replication lag in seconds.
    # Here we parse that "lag" field and keep the smallest value from among the active replicas.
    # Return false id there are no replcas or all replicas exceed our lag threshold.  Otherwise return true.
    #
    # Example matching lines from Redis INFO REPLICATION command output:
    #     slave0:ip=10.224.7.102,port=6379,state=online,offset=177621522296,lag=0
    #     slave1:ip=10.224.7.101,port=6379,state=online,offset=177621521081,lag=1

    BEST_REPLICATION_LAG=$(
        run_redis_command --raw info replication \
            | grep '^slave[0-9]*:.*,lag=' \
            | grep -o 'lag=[0-9]*' \
            | cut -d'=' -f2 \
            | sort -n \
            | head -n 1
    )
    if [[ $? -ne 0 ]] ; then
        log "Could not parse lag from redis INFO output.  Pessimistically assuming lag is unacceptable."
        return 1
    fi

    if [[ -z "$BEST_REPLICATION_LAG" ]] ; then
        log "Could not find any healthy replicas."
        return 1
    elif [[ "$BEST_REPLICATION_LAG" -gt "$MAX_ALLOWED_REPLICATION_LAG_SECONDS" ]] ; then
        log "All replicas exceed the target replication lag: $BEST_REPLICATION_LAG seconds > $MAX_ALLOWED_REPLICATION_LAG_SECONDS seconds"
        return 1
    else
        log "Replication lag is acceptable: $BEST_REPLICATION_LAG seconds"
        return 0
    fi
}

function skip_backup_if_recently_completed_last_successful_backup()
{
    # Emulate the behavior of the redis.conf config directive "SAVE <seconds> <changes>", where the next backup
    # is guaranteed to start no sooner than "<seconds>" after the completion of the last successful backup.
    #
    # This is not strictly necessary.  If a backup is already running (for any reason) when we try to
    # start a fresh backup, the new backup attempt will fail to start, and the ongoing backup will continue.
    #
    # The main motivation for implementing this minimum delay between backups is to match existing behavior,
    # to avoid unintuitive surprises.  For example, creating a manual backup will delay the next automated
    # backup, just as it would when using the redis.conf "save" directive.

    TIMESTAMP_NOW=$( date +%s )

    TIMESTAMP_LAST_BACKUP=$( run_redis_command --raw lastsave )
    if [[ $? -ne 0 ]] ; then
        log "Could not determine age of last successful backup.  Assuming it is old enough to replace."
        return
    fi

    LAST_BACKUP_AGE_SECONDS=$(( $TIMESTAMP_NOW - $TIMESTAMP_LAST_BACKUP ))
    if [[ "$LAST_BACKUP_AGE_SECONDS" -lt "$MIN_SECONDS_SINCE_LAST_SUCCESSFUL_BACKUP" ]] ; then
        skip_backup "Last successful backup is too new to replace: $LAST_BACKUP_AGE_SECONDS seconds old"
    fi
}

function run_backup()
{
    log "Starting a backup to run asynchronously."
    run_redis_command bgsave
    if [[ $? -ne 0 ]] ; then
        die "Failed to start backup."
    fi
}

function run_redis_command()
{
    [[ -n "$SKIP_CREDENTIAL_FETCH" ]] || [[ -n "$REDISCLI_AUTH" ]] || get_redis_credentials
    $REDIS_CLI "$@"
    [[ $? -eq 0 ]] || die "Failed to run redis-cli command with args: $@"
}

function get_redis_credentials()
{
    [[ -r "${REDIS_CONF}" ]] || die "Cannot read redis.conf."
    export REDISCLI_AUTH=$( grep -m 1 '^requirepass' ${REDIS_CONF} | cut -d' ' -f2 | tr -d \" )
    [[ -n "$REDISCLI_AUTH" ]] || die "Could not find redis credentials."
}

function skip_backup()
{
    local message=$1
    echo "$( date +%Y-%m-%d\ %H:%M:%S\ %Z )  [$$]  SKIP_BACKUP $message" 1>&2
    exit 1
}

function die()
{
    local message=$1
    echo "$( date +%Y-%m-%d\ %H:%M:%S\ %Z )  [$$]  ERROR $message" 1>&2
    exit 1
}

function log()
{
    local message=$1
    echo "$( date +%Y-%m-%d\ %H:%M:%S\ %Z )  [$$]  LOG $message"
}

main "$@"
