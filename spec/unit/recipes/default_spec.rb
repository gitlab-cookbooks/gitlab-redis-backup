# Cookbook:: gitlab_redis_backup
# Spec:: default
#
# Copyright:: 2017, GitLab B.V., MIT.

require 'spec_helper'

describe 'gitlab_redis_backup::default' do
  context 'when all attributes are default, on Ubuntu 16.04' do
    let(:chef_run) do
      ChefSpec::SoloRunner.new(
        platform: 'ubuntu',
        version: '16.04',
      ).converge(described_recipe)
    end

    it 'converges succesfully' do
      expect { chef_run }.to_not raise_error
    end

    it 'creates the /etc/gitlab directory' do
      expect(chef_run).to create_directory('/etc/gitlab')
    end

    it 'creates the conf file' do
      expect(chef_run).to render_file('/etc/gitlab/redis_backup.conf')
        .with_content('MAX_ALLOWED_REPLICATION_LAG_SECONDS=60')
        .with_content('MIN_SECONDS_SINCE_LAST_SUCCESSFUL_BACKUP=900')
        .with_content('REDIS_CLI=/opt/gitlab/embedded/bin/redis-cli')
        .with_content('REDIS_CONF=/var/opt/gitlab/redis/redis.conf')
    end

    it 'deploys the script' do
      expect(chef_run).to create_cookbook_file('/usr/local/bin/redis_backup.sh')
    end

    it 'creates systemd service units' do
      expect(chef_run).to create_systemd_unit('redis-rdb-backup.service')
      expect(chef_run).to enable_systemd_unit('redis-rdb-backup.service')

      expect(chef_run).to create_systemd_unit('redis-rdb-backup.timer')
      expect(chef_run).to enable_systemd_unit('redis-rdb-backup.timer')
      expect(chef_run).to start_systemd_unit('redis-rdb-backup.timer')
    end
  end

  context 'when timer is disabled on Ubuntu 16.04' do
    let(:chef_run) do
      ChefSpec::SoloRunner.new(
        platform: 'ubuntu',
        version: '16.04',
      ) do |node, _server|
        node.normal['gitlab_redis_backup']['systemd_timer']['enabled'] = false
      end.converge(described_recipe)
    end

    it 'converges succesfully' do
      expect { chef_run }.to_not raise_error
    end

    it 'creates the /etc/gitlab directory' do
      expect(chef_run).to create_directory('/etc/gitlab')
    end

    it 'creates systemd service units but disables and stops its timer' do
      expect(chef_run).to create_systemd_unit('redis-rdb-backup.service')
      expect(chef_run).to enable_systemd_unit('redis-rdb-backup.service')

      expect(chef_run).to create_systemd_unit('redis-rdb-backup.timer')
      expect(chef_run).to disable_systemd_unit('redis-rdb-backup.timer')
      expect(chef_run).to stop_systemd_unit('redis-rdb-backup.timer')
    end
  end

  context 'when skip_credential_fetch is enabled on Ubuntu 16.04' do
    let(:chef_run) do
      ChefSpec::SoloRunner.new(
        platform: 'ubuntu',
        version: '16.04',
      ) do |node, _server|
        node.normal['gitlab_redis_backup']['config_file']['skip_credential_fetch'] = true
      end.converge(described_recipe)
    end

    it 'converges succesfully' do
      expect { chef_run }.to_not raise_error
    end

    it 'creates the /etc/gitlab directory' do
      expect(chef_run).to create_directory('/etc/gitlab')
    end

    it 'creates the conf file' do
      expect(chef_run).to render_file('/etc/gitlab/redis_backup.conf')
        .with_content('MAX_ALLOWED_REPLICATION_LAG_SECONDS=60')
        .with_content('MIN_SECONDS_SINCE_LAST_SUCCESSFUL_BACKUP=900')
        .with_content('REDIS_CLI=/opt/gitlab/embedded/bin/redis-cli')
        .with_content('REDIS_CONF=/var/opt/gitlab/redis/redis.conf')
        .with_content('SKIP_CREDENTIAL_FETCH=true')
    end

    it 'deploys the script' do
      expect(chef_run).to create_cookbook_file('/usr/local/bin/redis_backup.sh')
    end

    it 'creates systemd service units' do
      expect(chef_run).to create_systemd_unit('redis-rdb-backup.service')
      expect(chef_run).to enable_systemd_unit('redis-rdb-backup.service')

      expect(chef_run).to create_systemd_unit('redis-rdb-backup.timer')
      expect(chef_run).to enable_systemd_unit('redis-rdb-backup.timer')
      expect(chef_run).to start_systemd_unit('redis-rdb-backup.timer')
    end
  end
end
