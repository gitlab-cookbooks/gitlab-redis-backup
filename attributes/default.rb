# Skip running a backup on the primary redis node if at least one replica is healthy and has replication lag
# below this threshold.
default['gitlab_redis_backup']['config_file']['max_allowed_replication_lag_seconds'] = 60

# Do not run backups too often.  Whether or not the backup was created by this script, avoid
# running a fresh backup if the last successful backup age is below this threshold.
# This emulates the Redis "SAVE <seconds> ..." directive.
# Note that it is safe for the cron schedule to be more frequent than this.  Extra runs become a noop.
default['gitlab_redis_backup']['config_file']['min_seconds_since_last_successful_backup'] = 900

# Path to the redis-cli executable.
default['gitlab_redis_backup']['config_file']['redis_cli'] = '/opt/gitlab/embedded/bin/redis-cli'

# Path to the "redis.conf" file for the running "redis-server" process.
# The redis credentials are extracted from this file.
default['gitlab_redis_backup']['config_file']['redis_conf'] = '/var/opt/gitlab/redis/redis.conf'

# Skips parsing redis_conf for credentials.
# Set to true when the cli executable script handles authentication.
default['gitlab_redis_backup']['config_file']['skip_credential_fetch'] = false

# Cron schedule for running the backup script.
# Note: Backups may take several minutes to complete, and if the last successful backup age is less than
# "min_seconds_since_last_successful_backup", then the script will skip making a fresher backup.
# So to converge towards making a backup roughly every X seconds, set this schedule to run often
# (e.g. every few minutes) and configure "min_seconds_since_last_successful_backup" to be X seconds.
default['gitlab_redis_backup']['systemd_timer']['on_calendar'] = '*-*-* *:*:25'

# Enable or disable the systemd timer (essentially the cronjob) that periodically runs the backup script.
default['gitlab_redis_backup']['systemd_timer']['enabled'] = true
