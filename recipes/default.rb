# Cookbook:: gitlab_redis_backup
# Recipe:: default
# License:: MIT
#
# Copyright:: 2021, GitLab Inc.
#
# Avoid running Redis RDB backups on primary under normal circumstances.

script_path = '/usr/local/bin/redis_backup.sh'
conf_file_path = '/etc/gitlab/redis_backup.conf'

directory '/etc/gitlab' do
  owner 'root'
  group 'root'
  mode '0775'
  action :create
end

template conf_file_path do
  source 'redis_backup.conf.erb'
  variables(
    max_allowed_replication_lag_seconds: node['gitlab_redis_backup']['config_file']['max_allowed_replication_lag_seconds'],
    min_seconds_since_last_successful_backup: node['gitlab_redis_backup']['config_file']['min_seconds_since_last_successful_backup'],
    redis_cli: node['gitlab_redis_backup']['config_file']['redis_cli'],
    redis_conf: node['gitlab_redis_backup']['config_file']['redis_conf'],
    skip_credential_fetch: node['gitlab_redis_backup']['config_file']['skip_credential_fetch'],
  )
  mode '0644'
  owner 'root'
  group 'root'
end

cookbook_file script_path do
  source 'redis_backup.sh'
  owner 'root'
  group 'root'
  mode '0755'
end

redis_backup_service_unit = {
  Unit: {
    Description: 'Timer-run service to periodically run Redis RDB backups, unless this node is currently the primary',
  },
  Service: {
    Type: 'oneshot',
    ExecStart: "#{script_path} #{conf_file_path}",
    User: 'root',
  },
}

redis_backup_timer_unit = {
  Unit: {
    Description: 'Timer to periodically run Redis RDB backups, unless this node is currently the primary',
  },
  Timer: {
    OnCalendar: node['gitlab_redis_backup']['systemd_timer']['on_calendar'],
    AccuracySec: '1s',
  },
  Install: {
    WantedBy: 'timers.target',
  },
}

systemd_unit 'redis-rdb-backup.service' do
  content redis_backup_service_unit
  action [:create, :enable]
end

running_action = node['gitlab_redis_backup']['systemd_timer']['enabled'] ? :start : :stop
enabled_action = node['gitlab_redis_backup']['systemd_timer']['enabled'] ? :enable : :disable

systemd_unit 'redis-rdb-backup.timer' do
  content redis_backup_timer_unit
  action [:create, enabled_action, running_action]
end
