# InSpec tests for recipe gitlab_redis_backup::default

control 'general-checks' do
  impact 1.0
  title 'General tests for gitlab_redis_backup cookbook'
  desc '
    This control ensures that:
      * all the basic files are created, with expected contents
      * services are enabled'

  describe file('/etc/gitlab/redis_backup.conf') do
    it { should be_file }
    its('owner') { should eq 'root' }
    its('group') { should eq 'root' }
    its('mode') { should cmp '0644' }
    its('content') { should match /^MAX_ALLOWED_REPLICATION_LAG_SECONDS=60$/ }
  end

  describe systemd_service('redis-rdb-backup.service') do
    it { should be_installed }
    it { should be_enabled }
    # Run by the timer, not as an active all-the-time service
    it { should_not be_running }
  end

  describe systemd_service('redis-rdb-backup.timer') do
    it { should be_enabled }
    it { should be_running }
  end
end
