[![build status](https://gitlab.com/gitlab-cookbooks/gitlab-redis-backup/badges/master/build.svg)](https://gitlab.com/gitlab-cookbooks/gitlab-redis-backup/commits/master)
[![coverage report](https://gitlab.com/gitlab-cookbooks/gitlab-redis-backup/badges/master/coverage.svg)](https://gitlab.com/gitlab-cookbooks/gitlab-redis-backup/commits/master)

# gitlab_redis_backup

Deploys the redis_backup.sh script and its timer task to a redis node.

This is intended to replace the native `SAVE <seconds> <count>` directive
in `redis.conf`.  Like that directive, this script runs `BGSAVE` to create
an RDB backup file.  However, because `BGSAVE` can cause a latency spike,
this script tries to avoid running it on the primary redis node.

The expected use case is to run this script on all nodes of a redis cluster,
and either remove or greatly reduce the frequency of the `SAVE` directive.

The intended effect is that RDB backups will be periodically created on the
replica redis nodes but not on the primary redis node.  The exception is
when all replicas are either down or have too great a replication lag,
then the primary node will have to make RDB backups until replication recovers.

## Using

A configuration file accompanies the script: `redis_backup.conf`

Settings include:
* `MAX_ALLOWED_REPLICATION_LAG_SECONDS` - At least one replica must have a replication lag
  less than this threshold.  Otherwise, the primary must create an RDB backup.
* `MIN_SECONDS_SINCE_LAST_SUCCESSFUL_BACKUP` - Cadence of backup creation.  Wait at least
  this long after the last successful backup before starting another one.  This emulates
  the behavior of the native Redis `SAVE <seconds> ...` directive.
* `REDIS_CLI` - Path to the redis-cli executable (typically `/opt/gitlab/embedded/bin/redis-cli`).
* `REDIS_CONF` - Path to the Redis config file (typically `/var/opt/gitlab/redis/redis.conf`).
  The redis credentials are extracted from this file.
* `SKIP_CREDENTIAL_FETCH` - Disables credential fetching from `REDIS_CONF`. To be used when
  no passwords are set or when the `REDIS_CLI` executable handle the authentication internally.
  This is usually the case if Redis ACL is used.

## Testing

The Makefile, which holds all the logic, is designed to be the same among all
cookbooks. Just set the comment at the top to include the cookbook name and
you are all set to use the below testing instructions.

### Testing locally

You can run `rspec` or `kitchen` tests directly without using provided
`Makefile`, although you can follow instructions to benefit from it.

1. Install GNU Make (`apt-get install make`). Under OS X you can achieve the
   same by `brew install make`. After this, you can see available targets of
   the Makefile just by running `make` in cookbook directory.

1. Cheat-sheet overview of current targets:

 * `make gems`: install latest version of required gems into directory,
   specified by environmental variable `BUNDLE_PATH`. By default it is set to
   the same directory as on CI, `.bundle`, in the same directory as Makefile
   is located.

 * `make lint`: find all `*.rb` files in the current directory, excluding ones
   in `BUNDLE_PATH`, and check them with rubocop and foodcritic.

 * `make rspec`: the above, plus run all the rspec tests. You can use
   `bundle exec rspec -f d` to skip the lint step, but it is required on CI
   anyways, so rather please fix it early ;)

 * `make kitchen`: calculate the number of suites in `.kitchen.do.yml`, and
   run all integration tests, using the calculated number as a `concurrency`
   parameter. In order to this locally by default, copy the example kitchen
   config to your local one: `cp .kitchen.do.yml .kitchen.local.yml`, or
   export environmental variable: `export KITCHEN_YAML=".kitchen.do.yml"`

   *Note* that `.kitchen.yml` is left as a default Vagrant setup and is not
   used by Makefile.

1. In order to use DigitalOcean for integration testing locally, by using
   `make kitchen` or running `bundle exec kitchen test --destroy=always`,
   export the following variables according to the
   [kitchen-digitalocean](https://github.com/test-kitchen/kitchen-digitalocean)
   documentation:
  * `DIGITALOCEAN_ACCESS_TOKEN`
  * `DIGITALOCEAN_SSH_KEY_IDS`
  To obtain values for these:
  * Look in 1password for "DIGITALOCEAN_ACCESS_TOKEN for gitlab-cookbooks CICD".
  * Upload your SSH key to the GitLab Dev project (where these builds will happen), then obtain the ID value with: `curl -s -S -X GET https://api.digitalocean.com/v2/account/keys/<fingerprint>  -H "Authorization: Bearer $DIGITALOCEAN_ACCESS_TOKEN" | jq .ssh_key.id`.  The fingerprint is visible in the web UI after upload, and can be copy/pasted as is into the API URL.  The ID returned is the value of DIGITALOCEAN_SSH_KEY_IDS; if the command outputs 'null', remove the pipe to jq, and see what the API is saying about why it can't find your SSH key.
  * If you're using a Yubikey, you probably also want to `export GITLAB_DO_SSH_KEY=` to set it explicitly to null, so it doesn't try and use a non-existent SSH key off disk, and just uses your ssh or gpg agent

### on CI

Alternatively, you can just push to your branch and let CI handle the testing.
`DIGITALOCEAN_ACCESS_TOKEN` should already be set as a CI variable at the group
level, so there is no need to separately set it for your project. The `make
kitchen` target will:
 * detect the CI environment
 * generate ephemeral SSH ed25519 keypair
 * register them on DigitalOcean
 * export the resulting key as `DIGITALOCEAN_SSH_KEY_IDS` environment variable
 * run the kitchen test
 * clean up the ephemeral key from DigitalOcean after pipeline is done

`.gitlab-ci.yml` is the source of truth for what actually runs in CI.
